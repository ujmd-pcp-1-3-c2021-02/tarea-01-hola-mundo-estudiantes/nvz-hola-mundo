﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo_Zizou_Navarrete
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo PCP 1-3";
            pictureBox1.Image = Properties.Resources.Icons;
            label1.BackColor = Color.Magenta;
            pictureBox1.BackColor = Color.Magenta;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo PCP 1-3";
            pictureBox1.Image = Properties.Resources.nerd;
            label1.BackColor = Color.DarkMagenta;
            pictureBox1.BackColor = Color.DarkMagenta;
        }
    }
}
